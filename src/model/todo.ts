export type TodoModel = {
    id: number,
    title: string,
    completed: boolean, 
}

const todos : TodoModel[] = [
    {
        id: 10001,
        title: 'Souhaiter un bon anniversaire sur le groupe What\'s App',
        completed: false,
    },
    {
        id: 10002,
        title: 'Boire un décaféiné',
        completed: true,
    },
    {
        id: 10003,
        title: 'Finir la présentation de React',
        completed: false,
    },
    {
        id: 10004,
        title: 'Montrer comment décrire un tuple en TypeScript',
        completed: false,
    },
    {
        id: 10005,
        title: 'Montrer comment décrire une fonction en TypeScript',
        completed: false,
    },
    {
        id: 10006,
        title: 'Fermer les fenêtres du bureau avant de partir',
        completed: true,
    },
]

export {todos};