import React, { useState, useEffect } from 'react';
import TodoList from './components/TodoList';
import './App.scss';
import Timer from './components/Timer';

export default function App() {

  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth); 

  const handleResize = () => {
    setWindowWidth(window.innerWidth);
  }

  useEffect(() => {
    window.addEventListener('resize', handleResize);
  
    return () => {
      window.removeEventListener('resize', handleResize);
    }
  }, []);

  return (
    <div className="App">
      <TodoList />
      {/* <div className="window">{windowWidth}</div> */}
      {/* <Timer /> */}
    </div>
  );
}
