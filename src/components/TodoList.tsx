import React, { useState } from 'react';
import {TodoModel, todos as todosDb} from '../model/todo'
import TodoItem from './TodoItem';
import './TodoList.scss'

 

export default function TodoList() {
    
    const [todos, setTodos] = useState<TodoModel[]>(todosDb); 
    // const count = todos.reduce((acc, t) => acc+(t.completed?1:0), 0);

    const handleCheck = (id:number) => {
        const newTodos = todos.map(todo => {
            if (todo.id === id ) {
                return {...todo, completed:!todo.completed};
            }
            return todo;
        });

        setTodos(newTodos);
    }

    const handleDelete = (id:number) => {
        const newTodos = todos.filter(todo => todo.id !== id);
        setTodos(newTodos);
    }

    return (
        <div className="TodoList">
            {todos.map((todo, index) => (
                <TodoItem
                    key={todo.id}
                    todo={todo}
                    onCheck={handleCheck} 
                    onDelete={handleDelete}
                />
            ))}
            {/* <span>{count}</span> */}
        </div>
    )
}