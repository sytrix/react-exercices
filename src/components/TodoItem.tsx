import React from 'react'
import { TodoModel } from '../model/todo';
import './TodoItem.scss'


interface TodoItemProps {
    todo: TodoModel,
    onCheck: (id:number)=>void,
    onDelete: (id:number)=>void,
}

export default function TodoItem({todo, onCheck, onDelete} : TodoItemProps) {

    const handleCheck = () => {
        onCheck(todo.id);
    }
    const handleDelete = () => {
        onDelete(todo.id);
    }

    return (
        <div 
            className="TodoItem" 
            id={`TodoItem_${todo.id}`}
        >
            <input type="checkbox" checked={todo.completed} onChange={handleCheck} />
            <div className="title">{todo.title}</div>
            <button onClick={handleDelete}>X</button>
        </div>
    )
}