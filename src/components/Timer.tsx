import React, { useEffect, useReducer, useRef, useState } from 'react'


interface Props {
}





type CounterState = {
    counter : number,
}

type Action = {
    type:'ADD',
    num: number,
} | {
    type:'SUB',
    num: number,
}

const init : CounterState = {
    counter: 0,
}

function reducer(state : CounterState, action : Action) {

    switch(action.type) {
        case 'ADD':
            return {...state, counter: state.counter + action.num};
        case 'SUB':
            return {...state, counter: state.counter - action.num};
        default:
            return state;
    } 


}

function useCounter() : [CounterState, ()=>void] {
    const [state, dispatch] = useReducer(reducer, init)
    const timeoutRef = useRef<NodeJS.Timeout|null>(null);

    const handleAdd = () => {
        dispatch({type: 'ADD', num:10});
    }

    useEffect(() => {
        timeoutRef.current = setTimeout(() => {
            dispatch({type: 'ADD', num:1});
        }, 2000);

        return () => {
            if (timeoutRef.current) {
                clearTimeout(timeoutRef.current);            
            }
        }
    }, [state.counter])

    return [state, handleAdd];
}


export default function Timer({} : Props) {

    const [state, handleAdd] = useCounter();

    
    return (
        <div className="Timer">
            {state.counter}
            <button onClick={handleAdd}>+10</button>
        </div>
    )
}
